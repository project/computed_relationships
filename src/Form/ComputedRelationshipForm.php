<?php

namespace Drupal\computed_relationships\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\computed_relationships\ComputedRelationshipInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ComputedRelationshipForm extends ContentEntityForm {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface|\Drupal\Core\Entity\RevisionLogInterface
   */
  protected $entity;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * @var CacheBackendInterface
   */
  protected $cache;

  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, CacheBackendInterface $cache) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->cache = $cache;

  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('cache.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $entity = $this->entity;

    // When this field is change, rebuild the form to update the target entities.
    $form['target_entity_bundle']['widget'] += [
      '#ajax' => [
        'callback' => '::updateTargetEntities',
        'wrapper' => 'target-entities-wrapper',
        'event' => 'change',
      ],
    ];

    $target_entity_bundle_value = $form_state->getValue('target_entity_bundle') ?: $entity->get('target_entity_bundle')->value;

    $form['target_entities']['widget']['#options'] = $this->getTargetEntitiesOptions($target_entity_bundle_value);
    $form['target_entities']['widget']['#default_value'] = array_column($entity->get('target_entities')->getValue(), 'value') ?? [];
    $form['target_entities']['widget'] += [
      '#prefix' => '<div id="target-entities-wrapper">',
      '#suffix' => '</div>',
      '#attributes' => ['style' => 'min-height: 200px; min-width: 300px'],
    ];

    return $form;
  }

  /**
   * Ajax callback to update the target_entities field.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The updated part of the form.
   */
  public function updateTargetEntities(array &$form, FormStateInterface $form_state) {
    return $form['target_entities'];
  }

  /**
   * Get options for the target entities select list.
   *
   * @param array|string $bundle
   *   The selected target entity bundle.
   *
   * @return array
   *   The options for the select list.
   */
  protected function getTargetEntitiesOptions($bundle) {
    if (empty($bundle)) {
      return ['_none' => $this->t('- Select -')];
    }

    if (is_array($bundle)) {
      $bundle = reset($bundle)['value'];
    }

    // Check the cache first.
    $cache_key = ComputedRelationshipInterface::OPTIONS_CACHE_KEY . "__$bundle";
    if ($cache = $this->cache->get($cache_key)) {
      return $cache->data;
    }

    [$entity_type_id, $bundle_id] = explode('__', $bundle);
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $entity_keys = $storage->getEntityType()->get('entity_keys');
    $entities = $storage->loadByProperties([$entity_keys['bundle'] => $bundle_id]);

    $options = ['_none' => $this->t('- Select -')];
    foreach ($entities as $entity) {
      $options[$entity->uuid()] = $entity->label();
    }

    if (count($options) === 1) {
      $options = ['_none' => $this->t('- No entities found -')];
    }

    // Cache the options.
    $this->cache->set($cache_key, $options);

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Disable entity validation to avoid issues with the target entities.
    $entity = $this->getEntity();
    $entity->setValidationRequired(FALSE);

    $fields = ['uid', 'created', 'source_entity_bundle', 'target_entity_bundle', 'target_entities'];
    foreach ($fields as $field) {
      $value = $form_state->getValue($field);
      while (is_array($value)) {
        $value = reset($value);
      }

      if (empty($value)) {
        $form_state->setErrorByName($field, $this->t('@field is required.', ['@field' => $form[$field]['widget'][0]['#title']]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->setValidationRequired(FALSE);

    // If label is empty, set "source_entity_bundle > target_entity_bundle" as label.
    if (empty($entity->label())) {
      $source_entity_bundle = $entity->get('source_entity_bundle')->value;
      $target_entity_bundle = $entity->get('target_entity_bundle')->value;
      $entity->set('label', "$source_entity_bundle > $target_entity_bundle");
    }

    if (empty($entity->get('computed_field_name')->value)) {
      $source_entity_bundle = $entity->get('source_entity_bundle')->value;
      $target_entity_bundle = $entity->get('target_entity_bundle')->value;
      $entity->set('computed_field_name', $source_entity_bundle . '__' . $target_entity_bundle);
    }

    $status = $entity->save();

    $message = match ($status) {
      SAVED_NEW => $this->t('Created the %label Computed Relationship.', ['%label' => $entity->label()]),
      default => $this->t('Saved the %label Computed Relationship.', ['%label' => $entity->label()]),
    };

    $this->messenger()->addMessage($message);

    $form_state->setRedirect('entity.computed_relationship.collection');
  }
}
