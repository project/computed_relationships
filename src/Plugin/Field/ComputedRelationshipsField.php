<?php

declare(strict_types=1);

namespace Drupal\computed_relationships\Plugin\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\ptur_term_municipality\VillageNoVillage;

/**
 * Computed field VillageList.
 */
class ComputedRelationshipsField extends EntityReferenceFieldItemList {

  use ComputedItemListTrait;

  /**
   * Compute the values.
   */
  protected function computeValue() {
    $entities = $this->getFieldDefinition()->getSetting('target_entities');
    $target_entity_type = $this->getFieldDefinition()->getSetting('target_type');
    $source_entity_type = $this->getFieldDefinition()->getSetting('source_type');
    $original = $this->getFieldDefinition()->getSetting('original');

     // Load all entities
    $entities = $this->removeDuplicates(
      $this->loadEntities($entities, $target_entity_type)
    );

    $entities = array_values($entities);

    if ($original->get('source_entity_bundle')->value !== $source_entity_type) {
      return;
    }

    foreach ($entities as $key => $entity) {
      $this->list[] = $this->createItem($key, $entity);
    }
  }

  protected function loadEntities($entities, $target_entity_type) {
    return array_map(function ($entity) use ($target_entity_type) {
      $entity = \Drupal::entityTypeManager()->getStorage($target_entity_type)
        ->loadByProperties(['uuid' => $entity['value']]);
      return reset($entity);
    }, $entities);
  }

  protected function removeDuplicates($entities) {
    $uuids = [];
    return array_filter($entities, function ($entity) use (&$uuids) {
      if (in_array($entity->uuid(), $uuids)) {
        return false;
      }
      $uuids[] = $entity->uuid();
      return true;
    });
  }

}
