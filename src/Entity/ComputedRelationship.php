<?php

namespace Drupal\computed_relationships\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\computed_relationships\ComputedRelationshipInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the computed relationship entity class.
 *
 * @ContentEntityType(
 *   id = "computed_relationship",
 *   label = @Translation("Computed relationship"),
 *   label_collection = @Translation("Computed relationships"),
 *   label_singular = @Translation("computed relationship"),
 *   label_plural = @Translation("computed relationships"),
 *   label_count = @PluralTranslation(
 *     singular = "@count computed relationships",
 *     plural = "@count computed relationships",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\computed_relationships\ComputedRelationshipListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\computed_relationships\Form\ComputedRelationshipForm",
 *       "edit" = "Drupal\computed_relationships\Form\ComputedRelationshipForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "computed_relationship",
 *   admin_permission = "administer computed relationship",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/computed-relationship",
 *     "add-form" = "/computed-relationship/add",
 *     "canonical" = "/computed-relationship/{computed_relationship}",
 *     "edit-form" = "/computed-relationship/{computed_relationship}/edit",
 *     "delete-form" = "/computed-relationship/{computed_relationship}/delete",
 *   },
 * )
 */
class ComputedRelationship extends ContentEntityBase implements ComputedRelationshipInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;


  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Label'))
        ->setSetting('max_length', 255)
        ->setDisplayOptions('form', [
            'type' => 'string_textfield',
            'weight' => -5,
        ])
        ->setDescription(t('The name of the computed relationship. If left blank, the label will be generated automatically.'))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayOptions('view', [
            'label' => 'hidden',
            'type' => 'string',
            'weight' => -5,
        ])
        ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t('Status'))
        ->setDefaultValue(TRUE)
        ->setSetting('on_label', 'Enabled')
        ->setDisplayOptions('form', [
            'type' => 'boolean_checkbox',
            'settings' => [
                'display_label' => FALSE,
            ],
            'weight' => 0,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayOptions('view', [
            'type' => 'boolean',
            'label' => 'above',
            'weight' => 0,
            'settings' => [
                'format' => 'enabled-disabled',
            ],
        ])
        ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Author'))
        ->setSetting('target_type', 'user')
        ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
        ->setDisplayOptions('form', [
            'type' => 'entity_reference_autocomplete',
            'settings' => [
                'match_operator' => 'CONTAINS',
                'size' => 60,
                'placeholder' => '',
            ],
            'weight' => 15,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'author',
            'weight' => 15,
        ])
        ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
        ->setLabel(t('Authored on'))
        ->setDescription(t('The time that the computed relationship was created.'))
        ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'timestamp',
            'weight' => 20,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayOptions('form', [
            'type' => 'datetime_timestamp',
            'weight' => 20,
        ])
        ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
        ->setLabel(t('Changed'))
        ->setDescription(t('The time that the computed relationship was last edited.'));

    $fields['computed_field_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Computed field name'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDescription(t('The machine name of the computed field. If left blank, the machine name will be generated automatically
      based on the source and target entity/bundle.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['source_entity_bundle'] = BaseFieldDefinition::create('list_string')
        ->setLabel(t('Source Entity/Bundle'))
        ->setRequired(TRUE)
        ->setSetting('allowed_values_function', [static::class, 'getEntityBundleOptions'])
        ->setDisplayOptions('form', [
            'type' => 'options_select',
            'weight' => 2,
        ])
        ->setDescription(t('Select the source entity and bundle.'))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

    $fields['target_entity_bundle'] = BaseFieldDefinition::create('list_string')
        ->setLabel(t('Target Entity/Bundle'))
        ->setRequired(TRUE)
        ->setSetting('allowed_values_function', [static::class, 'getEntityBundleOptions'])
        ->setDisplayOptions('form', [
            'type' => 'options_select',
            'weight' => 3,
        ])
        ->setDescription(t('Select the target entity and bundle. Only entities with at least one bundle will be available.'))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

    $fields['target_entities'] = BaseFieldDefinition::create('list_string')
        ->setLabel(t('Target Entities'))
        ->setRequired(TRUE)
        ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
        ->setSetting('allowed_values_function', [static::class, 'getDummyAllowedValues'])
        ->setDisplayOptions('form', [
            'type' => 'options_select',
            'weight' => 4
        ])
        ->setDescription(t('Select the target entities.'))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }
  /**
   * Provides allowed values for entity bundles.
   */
  public static function getEntityBundleOptions(BaseFieldDefinition $field) {
    $entity_types = \Drupal::entityTypeManager()->getDefinitions();
    $bundle_info = \Drupal::service('entity_type.bundle.info');
    $options = [];
    $field_name = $field->getFieldStorageDefinition()->getName();

    // Check if the options are already cached.
    $cache_key = self::OPTIONS_CACHE_KEY . '__' . $field_name;
    if ($cache = \Drupal::cache()->get($cache_key)) {
      return $cache->data;
    }

    foreach ($entity_types as $entity_type_id => $entity_type) {
      if ($entity_type_id === 'computed_relationship') {
        continue;
      }

      if ($entity_type->entityClassImplements('\Drupal\Core\Entity\ContentEntityInterface')) {
        $bundles = $bundle_info->getBundleInfo($entity_type_id);
        if (!empty($bundles)) {
          $optgroup_label = $entity_type->getLabel()->render();
          $optgroup_options = [];
          foreach ($bundles as $bundle_id => $bundle) {
            if ($field_name  === 'target_entity_bundle' && self::hasAnyEntities($entity_type_id, $bundle_id)) {
              $optgroup_options[$entity_type_id . '__' . $bundle_id] = $bundle['label'];
            }
            else if ($field_name === 'source_entity_bundle') {
              $optgroup_options[$entity_type_id . '__' . $bundle_id] = $bundle['label'];
            }
          }
          $options[$optgroup_label] = $optgroup_options;
        }
      }
    }

    $options = array_filter($options);

    // Cache the options.
    \Drupal::cache()->set($cache_key, $options);

    return $options;
  }

  public static function hasAnyEntities($entity_type_id, $bundle_id) {
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type_id);
    $entity_keys = $storage->getEntityType()->get('entity_keys');
    if (empty($entity_keys['bundle'])) {
      return FALSE;
    }
    $entities = $storage->getQuery()
      ->condition($entity_keys['bundle'], $bundle_id)
      ->range(0, 1)
      ->accessCheck(FALSE)
      ->execute();
    return count($entities) > 0;
  }

  /**
   * Caused by the cardinality -1 fields must have a default value with multiple values, is neccessary to provide
   * a dummy allowed values.
   */
  public static function getDummyAllowedValues() {
    return [
        '_none' => t('- Select target entity/bundle first -'),
        '_empty' => t('No entities found'),
        '_dummy' => 'If you see this, something went wrong.',
    ];

  }

}
