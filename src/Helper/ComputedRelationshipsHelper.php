<?php

namespace Drupal\computed_relationships\Helper;

use Drupal\computed_relationships\Entity\ComputedRelationship;
use Drupal\computed_relationships\Plugin\Field\ComputedRelationshipsField;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class ComputedRelationshipsHelper {
  use StringTranslationTrait;

  const TABLE_NAME = 'computed_relationship';
  const TARGET_ENTITIES = 'target_entities';
  const SOURCE_ENTITY_BUNDLE = 'source_entity_bundle';
  const TARGET_ENTITY_BUNDLE = 'target_entity_bundle';
  /**
   * The entity type manager.
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler.
   * @var ModuleHandlerInterface
   */
  protected $moduleHandler;

  public function __construct(EntityTypeManagerInterface $entityTypeManager, ConfigFactoryInterface $configFactory, ModuleHandlerInterface $moduleHandler) {
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $configFactory;
    $this->moduleHandler = $moduleHandler;
  }

  public function buildComputedFields(array $entities): array {
    $fields = [];

    foreach ($entities as $entity) {
      $computed_relationship = ComputedRelationship::load($entity);
      $target_entities = $computed_relationship->get(self::TARGET_ENTITIES)->getValue();
      $origin = $computed_relationship->get(self::SOURCE_ENTITY_BUNDLE)->value;
      $destination = $computed_relationship->get(self::TARGET_ENTITY_BUNDLE)->value;

      $name = $destination;
      list($target_entity_id, $target_bundle_id) = explode('__', $destination);

      $label = $computed_relationship->get('label')->value;


      $computed_fieldname = $computed_relationship->get('computed_field_name')->value;
      if (isset($fields[$computed_fieldname])) {
        $base_field = &$fields[$computed_fieldname];
        $target_entities = array_merge($base_field->getSetting(self::TARGET_ENTITIES), $target_entities);
        $base_field->setSetting(self::TARGET_ENTITIES, $target_entities);
      } else {
        $fields[$computed_fieldname] = BaseFieldDefinition::create('entity_reference')
          ->setLabel(t($label))
          ->setName($computed_fieldname)
          ->setDescription(t("Storage for $name"))
          ->setComputed(TRUE)
          ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
          ->setSetting('target_type', $target_entity_id)
          ->setSetting('target_entities', $target_entities)
          ->setSetting('source_type', $origin)
          ->setSetting('original', $computed_relationship)
          ->setDisplayConfigurable('view', TRUE)
          ->setTargetEntityTypeId($target_entity_id)
          ->setTargetBundle($target_bundle_id)
          ->setClass(ComputedRelationshipsField::class);
      }

      if ($this->moduleHandler->moduleExists('jsonapi_extras')) {
        $this->autoexport_jsonapi_extras_configuration($origin, $computed_fieldname);
      }
    }

    return $fields;
  }

  /**
   * Auto-export JSON:API Extras configuration. If the module is enabled, it will
   * automatically export the computed field configuration.
   *
   * @note: Is necessary that JSON:API extras `jsonapi_extras.jsonapi_resource_config.${entity__bundle}` configuration
   * exists in order to add the computed fields. If the computed field has already been added, it is not added
   * again or modified.
   *
   * @param string $origin
   * @param string $computed_fieldname
   * @return void
   */
  public function autoexport_jsonapi_extras_configuration(string $origin, string $computed_fieldname) {
    list($origin_entity_id, $origin_bundle_id) = explode('__', $origin);

    $bundle_info = \Drupal::service('entity_type.bundle.info');
    $bundles = $bundle_info->getBundleInfo($origin_entity_id);

    foreach ($bundles as $bundle => $bundle_label) {
      $disabled = ($origin_bundle_id !== $bundle);

      $formatted_name = "{$origin_entity_id}--{$bundle}";
      $base_config_name = "jsonapi_extras.jsonapi_resource_config.{$formatted_name}";
      $config = $this->configFactory->getEditable($base_config_name);
      $resource_fields = $config->get('resourceFields');

      if ($resource_fields !== NULL && !isset($resource_fields[$computed_fieldname])) {
        $resource_fields[$computed_fieldname] = [
          'disabled' => $disabled,
          'fieldName' => $computed_fieldname,
          'publicName' => $computed_fieldname,
          'enhancer' => ['id' => '']
        ];

        $config->set('resourceFields', $resource_fields);
        $config->save();
      }
    }
  }

  /**
   * This method checks if the computed relationship table exists in order to avoid errors during
   * the module activation.
   * @return bool
   */
  public static function tableExists(): bool {
    return \Drupal::database()->schema()->tableExists(ComputedRelationshipsHelper::TABLE_NAME);
  }
}
