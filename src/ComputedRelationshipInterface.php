<?php

namespace Drupal\computed_relationships;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a computed relationship entity type.
 */
interface ComputedRelationshipInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  const OPTIONS_CACHE_KEY = 'computed_relationship_entity_bundle_options';

}
